const { default: axios } = require("axios")

const router = require("express").Router()

const baseUrl = "https://learn-deploy-fsw.herokuapp.com"

router.get("/", (req, res) => {
  res.render("index", { page: "home" })
})

// books
router.get("/books", (req, res) => {
  axios.get(`${baseUrl}/books`)
  .then(result => {
    res.render("books/index", { title: "Data Buku", books: result.data.data, page: "books" })
  })
  .catch(err => console.log(err))
})

router.get("/books/create", (req, res) => {
  axios.get(`${baseUrl}/authors`)
  .then(result => {
    res.render("books/create", { title: "Tambah Buku", authors: result.data.data, page: "books" })
  })
})

router.post("/books", (req, res) => {
  axios.post(`${baseUrl}/books`, req.body)
  .then(result => {
    res.redirect("/books")
  })
  .catch(err => console.log(err))
})

module.exports = router