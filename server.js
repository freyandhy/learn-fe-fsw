const express = require("express")
const app = express()
const port = process.env.PORT || 4000

const routes = require("./routes")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.set("view engine", "ejs")
app.use(routes)

app.listen(port, () => console.log(`Server ready at ${port}`))